GitHub Repositories

Build and run server:
    Move to './server' and run 'npm i' and 'npm start'. 
    Server is under 3000 port.

Build and run client:
    Move to './client' and run 'npm i' and 'npm start'. 
    Client is under 4200 post.

Go to localhost:4200.
