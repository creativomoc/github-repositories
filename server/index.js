var express = require('express');
var app = express();
var requestify = require('requestify');



app.get('/repos', (req, res) => {

    const user = req.query && req.query.user ? req.query.user : null;

    if (user === null) {
        res.status(422).send(missingUserError);
        return;
    }

    requestify.get('https://api.github.com/users/' + user + '/repos').then(

        response => {
            const body = response.getBody();
            const data = body
                .sort((a, b) => b.stargazers_count - a.stargazers_count)
                .slice(0, 10)
                .map(item => {
                    return {
                        owner: item.owner.login,
                        avatar_url: item.owner.avatar_url,
                        name: item.name,
                        description: item.description,
                        language: item.language,
                        stars: item.stargazers_count,
                        forks: item.forks,
                    }
                })
            const result = {
                data: data
            }
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.send(result);
        },

        error => {
            const result = noSuchUserError;
            result.error += user;
            res.setHeader('Access-Control-Allow-Origin', '*');
            res.status(404).send(result);
        }
    );

});

const missingUserError = {
    error: 'Invalid/Missing user param'
}

const noSuchUserError = {
    error: 'No such user '
}

app.listen(3000, function () {
    console.log('App listening on port 3000');
});