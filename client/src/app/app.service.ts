import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs';

@Injectable()
export class AppService {

    private _loading: Boolean = false;
    public get loading(): Boolean { return this._loading; }
    public set loading(value: Boolean) { this._loading = value; }

    constructor(private http: Http) { }

    getRepositories(user: string): Observable<any[]> {
        this.loading = true;
        return this.http.get('http://localhost:3000/repos?user=' + user)
            .map(res => this.extractData(res))
            .catch(error => this.handleError(error))
    }

    private extractData(res: Response) {
        const body = res.json();
        this.loading = false;
        return body.data || {};
    }

    private handleError(error: Response | any) {
        this.loading = false;
        return Observable.throw(error);
    }
}