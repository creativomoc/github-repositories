import { Component, ViewEncapsulation, OnInit } from '@angular/core';
import { AppService } from './app.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit{

  private _repos: any[];
  public get repos() { return this._repos; }
  public set repos(value: any[]) { this._repos = value; }

  public get loading(): Boolean { return this.appService.loading; }

  private _userInput: string = 'johnpapa';
  public get userInput() { return this._userInput; }
  public set userInput(value: string) { this._userInput = value; }

  constructor(private appService: AppService) { }

  ngOnInit() {
    this.searchRepos();
  }

  public searchRepos() {
    const user = this.userInput;
    this.appService.getRepositories(user).subscribe(
      result => {
        this.repos = result;
      },
      error => {
        this.repos = null;
      }
    )
  }

  // View utils
  public get searchDisabled() {
    if(this.userInput === null || this.userInput === undefined) {
      return true;
    }
    const text = this.userInput.trim();
    if(text === '') {
      return true;
    }
    return false;
  }

}
