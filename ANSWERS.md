Q: How long did you spend on the coding test? What would you add to your solution if you had more time? If you didn't spend much time on the coding test then use this as an opportunity to explain what you would add.
R: I spent a couple of hours learning GitHub APIs, and a day (about 6 hours, 2 for the server and 4 for the client) to implement the solution.
There are a lot of small features easily implementable from the response of the search call, such as: direct links to the github user's profile or repository's page, 'has wiki' and 'watchers_count' infos to show.
I would have spent more time to make a better responsive grid, and make more cross-browser tests.
Furthermore, despite both the server and the client are very poor in logic, they should have the respective unit test.


Q: Please describe yourself using JSON.
R: {
    "name": "Gabriele",
    "surname": "Franchitto",
    "born": 611877600000,
    "age": 28,
    "degree": "Computer engineering, University of Bologna",
    "hobbies": ["music", "concerts", "technology"],
    "enthusiast": true,
    "reliable": true,
    "tolerant": true
}


Q: How would you improve the application's performance?
R: From a high level point of view, the application's performance for the current design can be improved with the exclusion of the server module. The server, used in this way is a simple proxy and it doesn't contain consistent logic. The client could use directly the github APIs in order to reduce the delay for the search requests.
Server-side performance can be probably improved using the github v4 GraphQl APIs (instead of v3 REST APIs). Furthermore, the used 'requestify' http client should be compared (in temrs of performance) with other http client libraries.
Client-side there is a very slow flow to use the only (search) functionality. This means that it's not necessary to use framework such as Angular or React to realize this project, it's possible to use Vanilla js to improve performance without wasting considerable hours for the implementation.

